// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "PickUps/STUPickUpBase.h"
#include "STUAmmoPickUp.generated.h"

/**
 *
 */

class ASTUWeaponBase;

UCLASS()
class STU_API ASTUAmmoPickUp : public ASTUPickUpBase
{
    GENERATED_BODY()
protected:

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PickUp", meta = (ClamMin = "1.0", ClampMax = "10.0"))
        int32 ClipsAmount = 10;

    UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "PickUp")
        TSubclassOf<ASTUWeaponBase> WeaponType;

private:

    virtual bool GivePickUpTo(APawn* PlayerPawn) override;
};
