// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STUPickUpBase.generated.h"

class USphereComponent;
class USoundCue;

UCLASS()
class STU_API ASTUPickUpBase : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ASTUPickUpBase();

protected:
    UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Components")
    USphereComponent* CollisionComponent = nullptr;

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Respawn")
    float RespawnTime = 3.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* PickUpTakenSound = nullptr;

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    bool CouldBeTaken() const;

private:
    FTimerHandle RespawnTimerHandle;

    float RotationYaw = 0.0f;

    virtual bool GivePickUpTo(APawn* PlayerPawn);
    void PickUpWasTaken();
    void Respawn();
    void GenerateRotationYaw();
};
