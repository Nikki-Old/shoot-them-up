// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "PickUps/STUPickUpBase.h"
#include "STUHealthPickUp.generated.h"

/**
 * 
 */
UCLASS()
class STU_API ASTUHealthPickUp : public ASTUPickUpBase
{
	GENERATED_BODY()
protected:

      UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "PickUp", meta = (ClampMin = 0.0f, ClampMax = 1000.0f))
      float HealthAmount;

  private:

      virtual bool GivePickUpTo(APawn* PlayerPawn) override;       
};
