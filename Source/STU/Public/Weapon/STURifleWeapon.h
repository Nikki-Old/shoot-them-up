// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/STUWeaponBase.h"
#include "STURifleWeapon.generated.h"

/**
 *
 */

class USTUWeaponFXComponent;
class UNiagaraComponent;
class UNiagaraSystem;
class UAudioComponent;

UCLASS()
class STU_API ASTURifleWeapon : public ASTUWeaponBase
{
    GENERATED_BODY()

public:
    ASTURifleWeapon();

    virtual void StartFire() override;
    virtual void StopFire() override;

    virtual void Zoom(bool Enabled) override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
    float TimeBetweenShots = 0.1f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
    float BulletSpread = 1.5f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
    FDamageEvent DamageEvent;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
    float Damage = 25.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    UNiagaraSystem* TraceFX;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    FString TraceTargetName = "TraceTarget";

    UPROPERTY(VisibleAnyWhere, Category = "VFX")
    USTUWeaponFXComponent* WeaponFXComponent = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
    float FOVZoomAngle = 50.f;

    virtual void BeginPlay() override;
    virtual void MakeShot() override;
    virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const override;

private:
    UPROPERTY(EditDefaultsOnly, Category = "Settings")
    FTimerHandle ShotTimerHandle;

    UPROPERTY()
    UNiagaraComponent* MuzzleFXComponent = nullptr;

    UPROPERTY()
    UAudioComponent* FireAudioComponent = nullptr;

    void MakeDamage(const FHitResult& HitResult);

    void InitFX();
    void SetFXActive(bool IsActive);

    void SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd);

    AController* GetController() const;

    float DefaultCameraFOV = 90.f;
};
