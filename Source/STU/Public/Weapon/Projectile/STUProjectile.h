// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STUProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class USTUWeaponFXComponent;

UCLASS()
class STU_API ASTUProjectile : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ASTUProjectile();

    void SetShotDirection(const FVector& Direction) { ShotDirection = Direction; }

protected:
    UPROPERTY(VisibleDefaultsOnly, Category = "Component's")
    USphereComponent* CollisionComponent = nullptr;
    UPROPERTY(VisibleDefaultsOnly, Category = "Component's")
    UProjectileMovementComponent* MovementComponent = nullptr;
    UPROPERTY(VisibleAnyWhere, Category = "Component's | VFX")
    USTUWeaponFXComponent* WeaponFXComponent = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
    float DamageRadius = 200.f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
    float Damage = 50.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    FColor SphereColor = FColor::Red;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    bool DoFullDamage = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    float LifeSeconds = 3.0f;
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

private:
    FVector ShotDirection;

    UFUNCTION()
    void OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
        const FHitResult& Hit);

    AController* GetController() const;
};
