// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/STUWeaponBase.h"
#include "STULauncherWeapon.generated.h"

/**
 *
 */
class ASTUProjectile;
class USoundCue;

UCLASS()
class STU_API ASTULauncherWeapon : public ASTUWeaponBase
{
    GENERATED_BODY()
public:
    virtual void StartFire() override;
    virtual void StopFire() override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
    TSubclassOf<ASTUProjectile> ProjectileClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* NoAmmoSound = nullptr;

    virtual void MakeShot() override;
};
