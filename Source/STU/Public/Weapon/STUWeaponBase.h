// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STUCoreTypes.h"
#include "STUWeaponBase.generated.h"

class USkeletaMeshComponent;
class UNiagaraSystem;
class UNiagaraComponent;
class USoundCue;

UCLASS()
class STU_API ASTUWeaponBase : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ASTUWeaponBase();

    FOnClipEmptySignature OnClipEmpty;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* FireSound = nullptr;

    virtual void StartFire();
    virtual void StopFire();

    void ChangeClip();
    bool CanReload();

    FWeaponUIData GetUIData() const { return UIData; }
    FAmmoData GetDefaultAmmoData() const { return DefaultAmmo; }
    FAmmoData GetCurrentAmmoData() const { return CurrentAmmo; }

    bool TryToAddAmmo(int32 ClipsAmount);
    bool IsAmmoEmpty() const;
    bool IsAmmoFull() const;

    virtual void Zoom(bool Enabled);

protected:
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Components")
    USkeletalMeshComponent* WeaponMesh;

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "MakeShot")
    FName MuzzleSocketName = "MuzzleFlashSocket";
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "MakeShot")
    float TraceMaxDistance = 1500.f;
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Ammo")
    FAmmoData DefaultAmmo{15, 10, false};

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "UI")
    FWeaponUIData UIData;

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "VFX")
    UNiagaraSystem* MuzzleFX = nullptr;


    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    virtual void MakeShot();
    virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const;

    void MakeHit(const FVector& TraceStart, const FVector& TraceEnd, FHitResult& HitResult) const;

    //APlayerController* GetPlayerController() const;
    bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const;
    FVector GetMuzzleWorldLocation() const;

    void DecreaseAmmo();
    bool IsClipEmpty() const;


    void LogAmmo();

    UNiagaraComponent* SpawnMuzzleFX();

private:
    FAmmoData CurrentAmmo;
};
