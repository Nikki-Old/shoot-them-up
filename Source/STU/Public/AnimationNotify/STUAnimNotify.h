// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "STUAnimNotify.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnNotifiedSignature, USkeletalMeshComponent*);

/**
 *  Base animation notify:
 */

UCLASS()
class STU_API USTUAnimNotify : public UAnimNotify
{
	GENERATED_BODY()

public:
    FOnNotifiedSignature OnNotifiedSignature;
    virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation);
};
