#pragma once

//  
class AnimUtils
{
public:
    template <typename T>
    static T* FindNotifyByClass(UAnimSequenceBase* Animation)
    {
        if (!Animation) return nullptr;

        // Take array Notify:
        const auto NotifyEvents = Animation->Notifies; 
        for (auto NotifyEvent : NotifyEvents) //...search Notify event in Notify array...
        {
            auto AnimNotify = Cast<T>(NotifyEvent.Notify);
            if (AnimNotify)
            {
                return AnimNotify;
            }
        }
        
        return nullptr;
    }
};
