// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "AnimationNotify/STUAnimNotify.h"
#include "STUReloadFinishedAnimNotify.generated.h"

/**
 *  Reload animation notify:
 */

UCLASS()
class STU_API USTUReloadFinishedAnimNotify : public USTUAnimNotify
{
	GENERATED_BODY()
	
};
