// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "STUCoreTypes.h"
#include "STUGameInstance.generated.h"

/**
 *
 */

class USoundClass;

UCLASS()
class STU_API USTUGameInstance : public UGameInstance
{
    GENERATED_BODY()

public:
    FString TestString = "Hello Game!";

    FLevelData GetStartupLevel() const { return StartupLevel; }
    void SetStartupLevel(const FLevelData& NewLevelData) { StartupLevel = NewLevelData; }

    TArray<FLevelData> GetLevelsData() const { return LevelsData; }

    FName GetMainMenuLevelName() const { return MainMenuLevelName; }

    void ToggleVolume();

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Game", meta = (ToolTip = "Level names must be unique! "));
    TArray<FLevelData> LevelsData;

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    FName MainMenuLevelName = NAME_None;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundClass* MasterSoundClass = nullptr;

    private:
    FLevelData StartupLevel;
};
