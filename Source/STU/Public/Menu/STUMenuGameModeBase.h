// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "STUMenuGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class STU_API ASTUMenuGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    ASTUMenuGameModeBase();
};
