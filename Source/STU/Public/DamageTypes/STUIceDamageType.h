// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "STUIceDamageType.generated.h"

/**
 *  Ice Damage type:
 */

UCLASS()
class STU_API USTUIceDamageType : public UDamageType
{
	GENERATED_BODY()
	
};
