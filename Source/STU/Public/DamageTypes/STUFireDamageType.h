// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "STUFireDamageType.generated.h"

/**
 *	Fire Damage types: 
 */

UCLASS()
class STU_API USTUFireDamageType : public UDamageType
{
	GENERATED_BODY()
	
};
