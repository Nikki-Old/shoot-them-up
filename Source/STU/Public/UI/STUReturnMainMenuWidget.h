// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "STUReturnMainMenuWidget.generated.h"

/**
 *
 */

class UButton;

UCLASS()
class STU_API USTUReturnMainMenuWidget : public UUserWidget
{
    GENERATED_BODY()

protected:
    UPROPERTY(meta = (BindWidget))
    UButton* ReturnMainMenuButton = nullptr;

    virtual void NativeOnInitialized() override;

private:
    UFUNCTION()
    void OnReturnMainMenu();
};
