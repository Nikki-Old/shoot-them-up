// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "STUPlayerStatRowWidget.generated.h"

/**
 *
 */

class UImage;
class UTextBlock;

UCLASS()
class STU_API USTUPlayerStatRowWidget : public UUserWidget
{
    GENERATED_BODY()
public:
    void SetPlayerName(const FText& Text);

    void SetKills(const FText& Text);

    void SetDeaths(const FText& Text);

    void SetTeam(const FText& Text);

    void SetPlayerIndicatorVisibility(const bool Visible);

    void SetTeamColor(const FLinearColor& Color);

protected:
    UPROPERTY(meta = (BindWidget))
    UTextBlock* PlayerNameTextBlock = nullptr;

    UPROPERTY(meta = (BindWidget))
    UTextBlock* KillsTextBlock = nullptr;

    UPROPERTY(meta = (BindWidget))
    UTextBlock* DeathsTextBlock = nullptr;

    UPROPERTY(meta = (BindWidget))
    UTextBlock* TeamTextBlock = nullptr;

    UPROPERTY(meta = (BindWidget))
    UImage* PlayerIndicatorImage = nullptr;

    UPROPERTY(meta = (BindWidget))
    UImage* TeamImage = nullptr;
};
