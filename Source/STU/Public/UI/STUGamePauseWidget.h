// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "UI/STUBaseWidget.h"
#include "STUGamePauseWidget.generated.h"

/**
 * 
 */

class UButton;

UCLASS()
class STU_API USTUGamePauseWidget : public USTUBaseWidget
{
	GENERATED_BODY()
public:
      virtual void NativeOnInitialized() override;

	  protected:
      UPROPERTY(meta = (BindWidget))
      UButton* ClearPauseButton = nullptr;

  private:
      UFUNCTION()
      void OnClearPause();
};
