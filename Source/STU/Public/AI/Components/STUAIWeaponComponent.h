// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Player/CharacterComponent/STUWeaponComponent.h"
#include "STUAIWeaponComponent.generated.h"

/**
 *
 */
UCLASS()
class STU_API USTUAIWeaponComponent : public USTUWeaponComponent
{
    GENERATED_BODY()

public:
    virtual void StartFire() override;
    virtual void NextWeapon() override;

};
