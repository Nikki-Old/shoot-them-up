// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "STUAmmoDecorator.generated.h"

/**
 *
 */

class ASTUWeaponBase;

UCLASS()
class STU_API USTUAmmoDecorator : public UBTDecorator
{
    GENERATED_BODY()

public:
    USTUAmmoDecorator();

protected:
    UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
    TSubclassOf<ASTUWeaponBase> WeaponType;

    /** calculates raw, core value of decorator's condition. Should not include calling IsInversed */
    virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
