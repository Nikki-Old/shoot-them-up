// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "STUHealthPercentDecorator.generated.h"

/**
 *
 */
UCLASS()
class STU_API USTUHealthPercentDecorator : public UBTDecorator
{
    GENERATED_BODY()

public:
    USTUHealthPercentDecorator();

protected:
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
    float HealthPercent = 0.6f;

    /** calculates raw, core value of decorator's condition. Should not include calling IsInversed */
    virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
