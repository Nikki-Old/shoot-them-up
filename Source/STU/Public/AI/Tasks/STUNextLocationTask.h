// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "STUNextLocationTask.generated.h"

/**
 *  This task calculates a new location point relative to the owner actor or the selected actor.
 */

UCLASS()
class STU_API USTUNextLocationTask : public UBTTaskNode
{
    GENERATED_BODY()

public:
    USTUNextLocationTask();

    virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
    /** In what radius is the selection of a new point location: */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    float Radius = 1000.f;

    /** Flag for take location point relative to the Owner actor: */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    bool SelfCentre = true;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FBlackboardKeySelector AimLocationKey;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI", meta = (EditContion = "!SelfCentre"))
    FBlackboardKeySelector CenterActorKey;
};
