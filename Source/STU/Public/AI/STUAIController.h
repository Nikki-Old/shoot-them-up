// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "STUAIController.generated.h"

/**
 *
 */

class USTUAIPerceptionComponent;
class USTURespawnComponent;

UCLASS()
class STU_API ASTUAIController : public AAIController
{
    GENERATED_BODY()

public:
    ASTUAIController();

protected:

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Component's")
    USTUAIPerceptionComponent* AIPerceptionComponent = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Component's")
    USTURespawnComponent* RespawnComponent = nullptr;

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
    FName FocusOnKeyName = "EnemyActor";

    // When a Pawn is under control of something:
    virtual void OnPossess(APawn* InPawn) override;
    
    virtual void Tick(float DeltaTime) override;

private:
    AActor* GetFocusOnActor() const;
};
