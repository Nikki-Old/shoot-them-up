// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Player/STUCharacterBase.h"
#include "STUAICharacter.generated.h"

/**
 *  Base AI Character:
 */

class UBehaviorTree;
class UWidgetComponent;

UCLASS()
class STU_API ASTUAICharacter : public ASTUCharacterBase
{
    GENERATED_BODY()

public:
    ASTUAICharacter(const FObjectInitializer& ObjInit);

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI")
    UBehaviorTree* BehaviorTreeAsset = nullptr;

    virtual void Tick(float DeltaTime) override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI")
    float HealthVisibilityDistance = 1000.f;

    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Components")
    UWidgetComponent* HealthWidgetComponent = nullptr;

    virtual void BeginPlay() override;

    virtual void OnDeath() override;

    virtual void OnHealthChange(float Health, float HealthDelta) override;

private:
    void UpdateHealthWidgetVisibility();
};
