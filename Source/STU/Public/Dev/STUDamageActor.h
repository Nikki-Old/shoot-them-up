// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STUDamageActor.generated.h"

UCLASS()
class STU_API ASTUDamageActor : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ASTUDamageActor();

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
    USceneComponent* SceneComponent;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    /** Sphere radius:*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    float Radius = 500.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    FColor SphereColor = FColor::Red;
    /** Damage in sphere radius: */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    float Damage = 0.1f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    bool DoFullDamage = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    TSubclassOf<UDamageType> DamageType;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
};
