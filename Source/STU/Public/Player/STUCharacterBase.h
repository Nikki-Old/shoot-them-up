// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "STUCharacterBase.generated.h"

class USTUHealthComponent;
// class UTextRenderComponent;
class USTUWeaponComponent;
class USoundCue;

UCLASS()
class STU_API ASTUCharacterBase : public ACharacter
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    ASTUCharacterBase(const FObjectInitializer& ObjInit);

protected:
    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Component's")
    USTUHealthComponent* HealthComponent = nullptr;

    // /** For display current health: */
    // UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Component's")
    // UTextRenderComponent* HealthTextComponent = nullptr;

    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Component's")
    USTUWeaponComponent* WeaponComponent = nullptr;

    UPROPERTY(EditDefaultsOnly, Category = "Animation")
    UAnimMontage* AnimDeath;

    // Landed logic:
    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Movement")
    FVector2D LandedDamageVelocity = FVector2D(900.f, 1200.f);
    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Movement")
    FVector2D LandedDamage = FVector2D(10.f, 100.f);

    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Team")
    FName MaterialColorName = "Paint Color";

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* DeathSound = nullptr;

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    virtual void OnDeath();

    virtual void OnHealthChange(float Health, float HealthDelta);

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintCallable, Category = "Movement")
    virtual bool IsSprinting() const;

    UFUNCTION(BlueprintCallable, Category = "Movement")
    float GetMovementDirection() const;

    void SetPlayerColor(const FLinearColor Color);

private:
    UFUNCTION()
    void OnGroundLanded(const FHitResult& HitResult);
};
