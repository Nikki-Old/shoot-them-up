// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Player/STUCharacterBase.h"
#include "STUPlayerCharacter.generated.h"

/**
 *
 */

class USpringArmComponent;
class UCameraComponent;
class USphereComponent;

UCLASS()
class STU_API ASTUPlayerCharacter : public ASTUCharacterBase
{
    GENERATED_BODY()
public:
    // Sets default values for this character's properties
    ASTUPlayerCharacter(const FObjectInitializer& ObjInit);

protected:
    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Component's")
    USpringArmComponent* SpringArmComponent = nullptr;

    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Component's")
    UCameraComponent* CameraComponent = nullptr;

    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Component's")
    USphereComponent* CameraCollisionComponent = nullptr;

    virtual void OnDeath() override;
    virtual void BeginPlay() override;

public:
    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    virtual bool IsSprinting() const override;

private:
    void MoveForward(float Amount);
    void MoveRight(float Amount);

    bool bIsSprint = false;
    bool bIsMovingForward = false;

    void StartSprint();
    void StopSprint();

    UFUNCTION()
    void OnCameraComponentBeginOverlap( UPrimitiveComponent* OverlappedComponent,
        AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

    UFUNCTION()
    void OnCameraComponentEndOverlap(UPrimitiveComponent* OverlappedComponent,
        AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

    void CheckCameraOverlap();
};
