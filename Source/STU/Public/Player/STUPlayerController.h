// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "STUCoreTypes.h"
#include "STUPlayerController.generated.h"

/**
 *  Base player controller:
 */

class USTURespawnComponent;

UCLASS()
class STU_API ASTUPlayerController : public APlayerController
{
    GENERATED_BODY()
public:
    ASTUPlayerController();

protected:
    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Component's")
    USTURespawnComponent* RespawnComponent = nullptr;

    virtual void BeginPlay() override;
    /** Allows the PlayerController to set up custom input bindings. */
    virtual void SetupInputComponent() override;

private:
    void OnPauseGame();

    void OnMatchStateChanged(ESTUMatchState State);
    
    void OnMuteSound();
};
