// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "STUCoreTypes.h"
#include "STUWeaponComponent.generated.h"

class ASTUWeaponBase;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class STU_API USTUWeaponComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    USTUWeaponComponent();

    virtual void StartFire();
    void StopFire();
    virtual void NextWeapon();
    void Reload();

    bool GetCurrentWeaponUIData(FWeaponUIData& UIData) const;
    bool GetCurrentWeaponAmmoData(FAmmoData& AmmoData, bool bNeedNumberClips = false) const;
    bool TryToAddAmmo(int32 ClipsAmount, TSubclassOf<ASTUWeaponBase> WeaponType);
    bool NeedAmmo(TSubclassOf<ASTUWeaponBase> WeaponType);

    void Zoom(bool Enabled);

protected:
    UPROPERTY(EditDefaultsOnly, Category = "SpawnWeapon")
    TArray<FWeaponData> WeaponData;

    UPROPERTY(EditDefaultsOnly, Category = "SpawnWeapon")
    FName WeaponEquipSocketName = "WeaponPoint";

    UPROPERTY(EditDefaultsOnly, Category = "SpawnWeapon")
    FName WeaponArmorySocketName = "ArmorySocket";

    UPROPERTY(EditDefaultsOnly, Category = "AnimationSwapWeapon")
    UAnimMontage* EquipAnimMontage = nullptr;

    UPROPERTY()
    ASTUWeaponBase* CurrentWeapon = nullptr;

    int32 CurrentWeaponIndex = 0;

    UPROPERTY()
    TArray<ASTUWeaponBase*> Weapons;

    // Called when the game starts
    virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

    bool CanFire() const;
    bool CanEquip() const;
    void EquipWeapon(int32 WeaponIndex);

private:
    UPROPERTY()
    UAnimMontage* CurrentReloadAnimMontage = nullptr;
    UPROPERTY()
    float ReloadAnimTime = 0.0f;

    FTimerHandle ReloadTimerHandle;

    bool bEquipAnimInProgress = false;
    bool bReloadAnimInProgress = false;

    void SpawnWeapons();
    void AttachWeaponToSocket(AActor* Weapon, USceneComponent* Mesh, const FName SocketName);

    float PlayAnimMontage(UAnimMontage* Animation);
    void InitAnimations();
    void OnEquipFinished(USkeletalMeshComponent* MeshComponent);
    void OnReloadFinished(USkeletalMeshComponent* MeshComponent);

    bool CanReload() const;

    void OnEmptyClip(ASTUWeaponBase* AmmoEmptyWeapon);
    void ChangeClip();
};
