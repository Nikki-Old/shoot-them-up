// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "STUCharacterMovementComponent.generated.h"

/**
 *  Character movement component:
 */

UCLASS()
class STU_API USTUCharacterMovementComponent : public UCharacterMovementComponent
{
    GENERATED_BODY()

public:
    // Overloading a dunction GetMaxSpeed:
    virtual float GetMaxSpeed() const override;

    /** Max speed * Sprint speed increase: */
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement", meta = (ClampMin = "1.5", ClampMax = "3"))
    float SprintSpeedIncrease = 1.5 ;
};
