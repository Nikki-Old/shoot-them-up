// Shoot Them Up Game. All Rights Reseved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "STUCoreTypes.h"
#include "STUHealthComponent.generated.h"

class UCameraShakeBase;
class UPhysicalMaterial;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class STU_API USTUHealthComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    USTUHealthComponent();

    // Return current Health:
    float GetHealth() const { return Health; };

    // Return true if haracter is dead.
    UFUNCTION(BlueprintCallable, Category = "Health")
    bool IsDead() const { return FMath::IsNearlyZero(Health); };

    // For widget:
    UFUNCTION(BlueprintCallable, Category = "Health")
    float GetHealthPercent() const { return Health / MaxHealth; };

    /** Return true if health is full. */
    UFUNCTION(BlueprintCallable, Category = "Health")
    bool IsHealthFull() const;

    // Check current health is not full and if not full set NewHealth:
    bool TryToAddHealth(float HealthAmount);

    // Delegates:
    FOnDeath OnDeath;
    FOnHealthChangedSignature OnHealthChange;

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = "1", ClampMax = "1000"))
    float MaxHealth = 100.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
    TMap<UPhysicalMaterial*, float> DamageModifiers;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AutoHeal")
    bool bAutoHeal = false;
    /** Delay before AutoHeal: */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AutoHeal", meta = (EditCondition = "bAutoHeal"))
    float HealDelay = 0.0f;
    /** How often add HealMultiplier ib DeltaTime: */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AutoHeal", meta = (EditCondition = "bAutoHeal"))
    float HealUpdateTime = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AutoHeal", meta = (EditCondition = "bAutoHeal"))
    float HealMultiplier = 0.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    TSubclassOf<UCameraShakeBase> CameraShake;

private:
    // Current health:
    float Health = 0.0f;

    FTimerHandle HealTimerHandle;

    /* float BufferHealDelay = 0.0f;
    float BufferHealUpdateTime = 0.0f;*/

    UFUNCTION()
    void OnTakeAnyDamage(
        AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

    UFUNCTION()
    void OnTakePointDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation,
        class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType,
        AActor* DamageCauser);

    UFUNCTION()
    void OnTakeRadialDanage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo,
        class AController* InstigatedBy, AActor* DamageCauser);

    UFUNCTION()
    void AutoHeal();

    UFUNCTION()
    void SetHealth(float NewHealth);

    void PlayCameraShake();

    void Killed(AController* KillerController);

    void ApplyDamage(float Damage, AController* InstigatedBy);

    float GetPointDamageModifier(AActor* DamageActor, const FName& BoneName);

    void ReportDamageEvent(float Damage, AController* InstigatedBy);
};
