// Shoot Them Up Game. All Rights Reseved.

using UnrealBuildTool;

public class STU : ModuleRules
{
    public STU(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Niagara", "PhysicsCore", "GameplayTasks", "NavigationSystem" });

        PrivateDependencyModuleNames.AddRange(new string[] { });

        PublicIncludePaths.AddRange(new string[] { "STU/Public/Player", "STU/Public/UI", "STU/Public/GameMode", "STU/Public/Weapon", "STU/Public/Weapon/AnimNotify", "STU/Public/Weapon/FX", "STU/Public/AI/Tasks", "STU/Public/AI/Services", "STU/Public/AI/Decorators", "STU/Public/PickUps", "STU/Public/Menu", "STU/Public/Menu/UI", "STU/Public/Sound" });



        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
