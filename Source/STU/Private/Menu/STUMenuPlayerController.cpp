// Shoot Them Up Game. All Rights Reseved.


#include "Menu/STUMenuPlayerController.h"
#include "STUGameInstance.h"

void ASTUMenuPlayerController::BeginPlay()
{
    Super::BeginPlay();
    
    SetInputMode(FInputModeUIOnly());
    bShowMouseCursor = true;

    GetWorld()->GetGameInstance<USTUGameInstance>()->TestString = "Hello Game in Menu!";
}