// Shoot Them Up Game. All Rights Reseved.


#include "UI/STUSpectatorWidget.h"
#include "STUUtils.h"
#include "CharacterComponent/STURespawnComponent.h"

bool USTUSpectatorWidget::GetRespawnTime(int32& CountTimeDown)
{
    const auto RespawnComponent = STUUtils::GetSTUPlayerComponent<USTURespawnComponent>(GetOwningPlayer());
    if (!RespawnComponent || !RespawnComponent->IsRespawnInProgress()) return false;
    
    CountTimeDown = RespawnComponent->GetRespawnCountDown();
    return true;
}