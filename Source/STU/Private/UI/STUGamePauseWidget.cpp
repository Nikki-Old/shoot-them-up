// Shoot Them Up Game. All Rights Reseved.

#include "UI/STUGamePauseWidget.h"
#include "Gameframework/GameModeBase.h"
#include "Components/Button.h"

void USTUGamePauseWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (ClearPauseButton)
    {
        ClearPauseButton->OnClicked.AddDynamic(this, &USTUGamePauseWidget::OnClearPause);
    }
}
void USTUGamePauseWidget::OnClearPause()
{
    if (!GetWorld() || !GetWorld()->GetAuthGameMode()) return;

    GetWorld()->GetAuthGameMode()->ClearPause();
}
