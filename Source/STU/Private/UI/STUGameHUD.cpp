// Shoot Them Up Game. All Rights Reseved.

#include "UI/STUGameHUD.h"
#include "Engine/Canvas.h"
#include "UI/STUBaseWidget.h"
#include "STUGameModeBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogSTUGameHUD, All, All);

void ASTUGameHUD::DrawHUD()
{
    Super::DrawHUD();

    ///** Draws crosshaur: */
    // DrawCrossHUD();
}
void ASTUGameHUD::BeginPlay()
{
    Super::BeginPlay();

    /* auto PlayerHUDWidget = CreateWidget<STUBaseWidget>(GetWorld(), PlayerHUDWidgetClass);
    if (PlayerHUDWidget)
    {
        PlayerHUDWidget->AddToViewport();
    }*/
    // AddNewWidget(PlayerHUDWidgetClass);

    GameWidgets.Add(ESTUMatchState::InProgress, AddNewWidget(PlayerHUDWidgetClass));
    GameWidgets.Add(ESTUMatchState::Pause, AddNewWidget(PauseWidgetClass));
    GameWidgets.Add(ESTUMatchState::GameOver, AddNewWidget(GameOverWidgetClass));

    for (auto GameWidgetPair : GameWidgets)
    {
        const auto GameWidget = GameWidgetPair.Value;
        if (!GameWidget) continue;

        GameWidget->AddToViewport();
        GameWidget->SetVisibility(ESlateVisibility::Hidden);
    }

    if (GetWorld())
    {
        const auto GameMode = Cast<ASTUGameModeBase>(GetWorld()->GetAuthGameMode());
        if (GameMode)
        {
            GameMode->OnMatchStateChanged.AddUObject(this, &ASTUGameHUD::OnMatchStateChanged);
        }
    }
}

/** Draws crosshaur: */
void ASTUGameHUD::DrawCrossHUD()
{
    const TInterval<float> Center(Canvas->SizeX * 0.5f, Canvas->SizeY * 0.5f);
    const float HalfLineSize = 10.f;
    const float LineThickness = 2.0f;
    const FLinearColor LineColor = FColor::Red;

    DrawLine(Center.Min - HalfLineSize, Center.Max, Center.Min + HalfLineSize, Center.Max, LineColor, LineThickness);
    DrawLine(Center.Min, Center.Max - HalfLineSize, Center.Min, Center.Max + HalfLineSize, LineColor, LineThickness);
}

void ASTUGameHUD::OnMatchStateChanged(ESTUMatchState State)
{
    if (CurrentWidget)
    {
        CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
    }

    if (GameWidgets.Contains(State))
    {
        CurrentWidget = GameWidgets[State];
    }

    if (CurrentWidget)
    {
        CurrentWidget->SetVisibility(ESlateVisibility::Visible);
        CurrentWidget->Show();
    }
    UE_LOG(LogSTUGameHUD, Warning, TEXT("Match state changed: %s"), *UEnum::GetValueAsString(State));
}

USTUBaseWidget* ASTUGameHUD::AddNewWidget(TSubclassOf<USTUBaseWidget> WidgetClass)
{
    if (!GetWorld()) return nullptr;

    auto Widget = CreateWidget<USTUBaseWidget>(GetWorld(), WidgetClass);
    if (!Widget) return nullptr;

    Widget->AddToViewport();

    return Widget;
}
