// Shoot Them Up Game. All Rights Reseved.


#include "UI/STUReturnMainMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "STUGameInstance.h"

DEFINE_LOG_CATEGORY_STATIC(LogSTUReturnMainMenuWidget, All, All);

void USTUReturnMainMenuWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (ReturnMainMenuButton)
    {
        ReturnMainMenuButton->OnClicked.AddDynamic(this, &USTUReturnMainMenuWidget::OnReturnMainMenu);
    }
}

void USTUReturnMainMenuWidget::OnReturnMainMenu()
{
    if (!GetWorld()) return;

    const auto STUGameInstance = GetWorld()->GetGameInstance<USTUGameInstance>();
    if (!STUGameInstance) return;

    if (STUGameInstance->GetMainMenuLevelName().IsNone())
    {
        UE_LOG(LogSTUReturnMainMenuWidget, Error, TEXT("USTUReturnMainMenuWidget::OnReturnMainMenu: MainMenuLevelName is None!"));
        return;
    }

    UGameplayStatics::OpenLevel(this, STUGameInstance->GetMainMenuLevelName());
}
