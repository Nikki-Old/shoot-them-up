// Shoot Them Up Game. All Rights Reseved.

#include "UI/STUBaseWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

void USTUBaseWidget::Show()
{
    PlayAnimation(ShowAnimation);
    if (GetWorld()) UGameplayStatics::PlaySound2D(GetWorld(), OpenSound);
}