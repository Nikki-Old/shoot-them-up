// Shoot Them Up Game. All Rights Reseved.

#include "AI/Services/STUChangeWeaponService.h"
#include "AI/Components/STUAIWeaponComponent.h"
#include "AIController.h"
#include "STUUtils.h"

USTUChangeWeaponService::USTUChangeWeaponService()
{
    NodeName = "Change Weapon";
}

void USTUChangeWeaponService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
    const auto Controller = OwnerComp.GetAIOwner();

    float Result = FMath::FRand();
    if (Controller)
    {
        if (Probability >= Result && Result > 0.f)
        {
            const auto WeaponComponent = STUUtils::GetSTUPlayerComponent<USTUAIWeaponComponent>(Controller->GetPawn());
            if (WeaponComponent)
            {
                WeaponComponent->NextWeapon();
            }
        }
    }

    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}