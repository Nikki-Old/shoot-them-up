// Shoot Them Up Game. All Rights Reseved.

#include "AI/Decorators/STUAmmoDecorator.h"
#include "CharacterComponent/STUHealthComponent.h"
#include "CharacterComponent/STUWeaponComponent.h"
#include "STUUtils.h"
#include "AIController.h"

USTUAmmoDecorator::USTUAmmoDecorator()
{
    NodeName = "Need Ammo";
}

bool USTUAmmoDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
    const auto Controller = OwnerComp.GetAIOwner();
    if (!Controller) return false;

    const auto HealthComponent = STUUtils::GetSTUPlayerComponent<USTUHealthComponent>(Controller->GetPawn());
    if (!HealthComponent || HealthComponent->IsDead()) return false;

    const auto WeaponComponent = STUUtils::GetSTUPlayerComponent<USTUWeaponComponent>(Controller->GetPawn());
    //if (!WeaponComponent || WeaponComponent->CanFire()) return false;
    if (!WeaponComponent) return false;

    return WeaponComponent->NeedAmmo(WeaponType);
}
