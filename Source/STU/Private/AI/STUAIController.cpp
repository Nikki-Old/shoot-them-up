// Shoot Them Up Game. All Rights Reseved.

#include "AI/STUAIController.h"
#include "AI/STUAICharacter.h"
#include "AI/Components/STUAIPerceptionComponent.h"
#include "CharacterComponent/STURespawnComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

ASTUAIController::ASTUAIController()
{
    AIPerceptionComponent = CreateDefaultSubobject<USTUAIPerceptionComponent>("AIPerceptionComponent");
    SetPerceptionComponent(*AIPerceptionComponent);

    RespawnComponent = CreateDefaultSubobject<USTURespawnComponent>("RespawnComponent");
    bWantsPlayerState = true;
}

// When a Pawn is under control of something:
void ASTUAIController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    // Set BehaviorTree:
    const auto STUCharacter = Cast<ASTUAICharacter>(InPawn);
    if (STUCharacter)
    {
        RunBehaviorTree(STUCharacter->BehaviorTreeAsset);
    }
}

void ASTUAIController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    // Set Focus all time:
    SetFocus(GetFocusOnActor());
}

AActor* ASTUAIController::GetFocusOnActor() const
{
    if (!GetBlackboardComponent()) return nullptr;
    
    return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}
