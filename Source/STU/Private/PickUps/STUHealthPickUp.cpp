// Shoot Them Up Game. All Rights Reseved.


#include "PickUps/STUHealthPickUp.h"
#include "Player/CharacterComponent/STUHealthComponent.h"
#include "STUUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthPickUp, All, All);

bool ASTUHealthPickUp::GivePickUpTo(APawn* PlayerPawn)
{
    // Find health component:
    const auto HealthComponent = STUUtils::GetSTUPlayerComponent<USTUHealthComponent>(PlayerPawn);
    // Check character is not dead and health is not full?
    if (!HealthComponent || HealthComponent->IsDead() || HealthComponent->IsHealthFull()) return false;
    // Call function added health:
    HealthComponent->TryToAddHealth(HealthAmount);

    UE_LOG(LogHealthPickUp, Display, TEXT("Health was taken"));
    return true;
}
