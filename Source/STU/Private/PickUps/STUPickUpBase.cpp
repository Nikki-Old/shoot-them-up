// Shoot Them Up Game. All Rights Reseved.

#include "PickUps/STUPickUpBase.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickUpBase, All, All);

// Sets default values
ASTUPickUpBase::ASTUPickUpBase()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    CollisionComponent = CreateDefaultSubobject<USphereComponent>("CollisionComponent");
    CollisionComponent->InitSphereRadius(50.f);
    CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    SetRootComponent(CollisionComponent);
}

// Called when the game starts or when spawned
void ASTUPickUpBase::BeginPlay()
{
    Super::BeginPlay();

    check(CollisionComponent);

    GenerateRotationYaw();
}

void ASTUPickUpBase::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);

    const auto Pawn = Cast<APawn>(OtherActor);
    if (!Pawn) return;

    if (GivePickUpTo(Pawn))
    {
        if (GetWorld()) UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickUpTakenSound, GetActorLocation());

        UE_LOG(LogPickUpBase, Display, TEXT("Begin overlap with %s"), *OtherActor->GetName());

        PickUpWasTaken();
    }
}

// Called every frame
void ASTUPickUpBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    AddActorLocalRotation(FRotator(0.0f, RotationYaw, 0.0f));
}

bool ASTUPickUpBase::CouldBeTaken() const
{
    return !GetWorldTimerManager().IsTimerActive(RespawnTimerHandle);
}

bool ASTUPickUpBase::GivePickUpTo(APawn* PlayerPawn)
{
    return false;
}

void ASTUPickUpBase::PickUpWasTaken()
{
    CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    if (GetRootComponent())
    {
        GetRootComponent()->SetVisibility(false, true);
    }

    GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &ASTUPickUpBase::Respawn, RespawnTime, false);
}

void ASTUPickUpBase::Respawn()
{
    GenerateRotationYaw();

    GetRootComponent()->SetVisibility(true, true);
    CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

void ASTUPickUpBase::GenerateRotationYaw()
{
    const auto Direction = FMath::RandBool() ? 1.0f : -1.0f;
    RotationYaw = FMath::FRandRange(1.0f, 2.0f) * Direction;
}
