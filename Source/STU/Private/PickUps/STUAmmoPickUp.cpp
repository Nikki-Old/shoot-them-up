// Shoot Them Up Game. All Rights Reseved.


#include "PickUps/STUAmmoPickUp.h"
#include "Weapon/STUWeaponBase.h"
#include "Player/CharacterComponent/STUWeaponComponent.h"
#include "Player/CharacterComponent/STUHealthComponent.h"
#include "STUUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogAmmoPickUp, All, All);

bool ASTUAmmoPickUp::GivePickUpTo(APawn* PlayerPawn)
{
    // Find health component:
    const auto HealthComponent = STUUtils::GetSTUPlayerComponent<USTUHealthComponent>(PlayerPawn);
    // Check chracrater is not dead ?
    if (!HealthComponent || HealthComponent->IsDead()) return false;

    // Find weapon component:
    const auto WeaponComponent = STUUtils::GetSTUPlayerComponent<USTUWeaponComponent>(PlayerPawn);
    if (!WeaponComponent) return false;

    UE_LOG(LogAmmoPickUp, Display, TEXT("Ammo was taken"));
    // call function get ammo:
    return WeaponComponent->TryToAddAmmo(ClipsAmount, WeaponType);
}
