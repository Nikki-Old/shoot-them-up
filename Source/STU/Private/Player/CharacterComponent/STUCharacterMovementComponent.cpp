// Shoot Them Up Game. All Rights Reseved.

#include "Player/CharacterComponent/STUCharacterMovementComponent.h"
#include "Player/STUCharacterBase.h"

// Overloading a dunction GetMaxSpeed:
float USTUCharacterMovementComponent::GetMaxSpeed() const 
{
    const float MaxSpeed = Super::GetMaxSpeed();
    const ASTUCharacterBase* Player = Cast<ASTUCharacterBase>(GetPawnOwner());
    
    return Player && Player->IsSprinting() ? MaxSpeed * SprintSpeedIncrease : MaxSpeed;
}
