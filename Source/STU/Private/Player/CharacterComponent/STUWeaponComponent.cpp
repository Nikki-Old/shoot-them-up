// Shoot Them Up Game. All Rights Reseved.

#include "Player/CharacterComponent/STUWeaponComponent.h"
#include "Weapon/STUWeaponBase.h"
#include "Weapon/AnimNotify/STUEquipFinishedAnimNotify.h"
#include "GameFrameWork/Character.h"
#include "Animation/AnimMontage.h"
#include "AnimationNotify/STUReloadFinishedAnimNotify.h"
#include "AnimationNotify/AnimUtils.h"
#include "TimerManager.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponComponent, All, All);

constexpr static int32 WeaponNum = 2;

// Sets default values for this component's properties
USTUWeaponComponent::USTUWeaponComponent()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;

    // ...
}

// Called when the game starts
void USTUWeaponComponent::BeginPlay()
{
    Super::BeginPlay();

    checkf(WeaponData.Num() == WeaponNum, TEXT("Our character can hold only %i weapon items!"), WeaponNum);

    CurrentWeaponIndex = 0;
    InitAnimations();
    SpawnWeapons();
    EquipWeapon(CurrentWeaponIndex);
}

void USTUWeaponComponent::StartFire()
{
    if (!CanFire()) return;

    CurrentWeapon->StartFire();
}

void USTUWeaponComponent::StopFire()
{
    if (!CurrentWeapon) return;

    CurrentWeapon->StopFire();
}

void USTUWeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    CurrentWeapon = nullptr;
    for (auto Weapon : Weapons)
    {
        Weapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
        Weapon->Destroy();
    }
    Weapons.Empty();

    Super::EndPlay(EndPlayReason);
}

void USTUWeaponComponent::SpawnWeapons()
{
    ACharacter* Character = Cast<ACharacter>(GetOwner());
    if (!Character || !GetWorld()) return;

    for (auto OneWeaponData : WeaponData)
    {
        auto Weapon = GetWorld()->SpawnActor<ASTUWeaponBase>(OneWeaponData.WeaponClass);
        if (!Weapon) continue;

        Weapon->OnClipEmpty.AddUObject(this, &USTUWeaponComponent::OnEmptyClip);
        Weapon->SetOwner(GetOwner());
        Weapons.Add(Weapon);

        AttachWeaponToSocket(Weapon, Character->GetMesh(), WeaponArmorySocketName);
    }
}

void USTUWeaponComponent::AttachWeaponToSocket(AActor* Weapon, USceneComponent* Mesh, const FName SocketName)
{
    if (!Weapon || !Mesh) return;
    FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
    Weapon->AttachToComponent(Mesh, AttachmentRules, SocketName);
}

void USTUWeaponComponent::EquipWeapon(int32 WeaponIndex)
{
    if (WeaponIndex < 0 || WeaponIndex >= Weapons.Num()) return;
    ACharacter* Character = Cast<ACharacter>(GetOwner());
    if (!Character) return;

    if (CurrentWeapon)
    {
        CurrentWeapon->StopFire();
        CurrentWeapon->Zoom(false);

        AttachWeaponToSocket(CurrentWeapon, Character->GetMesh(), WeaponArmorySocketName);
    }

    if (Weapons.IsValidIndex(WeaponIndex))
    {
        CurrentWeapon = Weapons[WeaponIndex];
        // CurrentReloadAnimMontage = WeaponData[WeaponIndex].ReloadAnimMontage;
        const auto CurrentWeaponData = WeaponData.FindByPredicate([&](const FWeaponData& Data)  //
            { return Data.WeaponClass == CurrentWeapon->GetClass(); });                         //
        CurrentReloadAnimMontage = CurrentWeaponData ? CurrentWeaponData->ReloadAnimMontage : nullptr;
        AttachWeaponToSocket(CurrentWeapon, Character->GetMesh(), WeaponEquipSocketName);
    }
    else
    {
        UE_LOG(LogWeaponComponent, Error, TEXT("EquipWeapon: Index is not valid %i"), WeaponIndex);
    }

    bEquipAnimInProgress = true;
    PlayAnimMontage(EquipAnimMontage);
}

void USTUWeaponComponent::NextWeapon()
{
    if (!CanEquip()) return;

    CurrentWeaponIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
    EquipWeapon(CurrentWeaponIndex);
}

void USTUWeaponComponent::Reload()
{
    if (CanReload())
    {
        ChangeClip();
    }
}

float USTUWeaponComponent::PlayAnimMontage(UAnimMontage* Animation)
{
    ACharacter* Character = Cast<ACharacter>(GetOwner());
    if (!Character) return 0.0f;

    return Character->PlayAnimMontage(Animation);
}

void USTUWeaponComponent::InitAnimations()
{
    auto EquipFinishedNotify = AnimUtils::FindNotifyByClass<USTUEquipFinishedAnimNotify>(EquipAnimMontage);
    if (EquipFinishedNotify)
    {
        EquipFinishedNotify->OnNotifiedSignature.AddUObject(this, &USTUWeaponComponent::OnEquipFinished);
    }
    else
    {
        UE_LOG(LogWeaponComponent, Error, TEXT("Equip animation notify is forgotten to set"));
        checkNoEntry();
    }

    for (auto OneWeaponData : WeaponData)
    {
        auto ReloadFinishedNotify = AnimUtils::FindNotifyByClass<USTUReloadFinishedAnimNotify>(OneWeaponData.ReloadAnimMontage);
        if (!ReloadFinishedNotify)
        {
            UE_LOG(LogWeaponComponent, Error, TEXT("Reload animation notify is forgotten to set"));
            checkNoEntry();
        }

        ReloadFinishedNotify->OnNotifiedSignature.AddUObject(this, &USTUWeaponComponent::OnReloadFinished);
    }
}

void USTUWeaponComponent::OnEquipFinished(USkeletalMeshComponent* MeshComponent)
{
    ACharacter* Character = Cast<ACharacter>(GetOwner());
    if (!Character || Character->GetMesh() != MeshComponent) return;

    // UE_LOG(LogWeaponComponent, Display, TEXT("Equip finished"));
    bEquipAnimInProgress = false;
}

void USTUWeaponComponent::OnReloadFinished(USkeletalMeshComponent* MeshComponent)
{
    ACharacter* Character = Cast<ACharacter>(GetOwner());
    if (!Character || Character->GetMesh() != MeshComponent) return;

    // UE_LOG(LogWeaponComponent, Display, TEXT("Equip finished"));
    bReloadAnimInProgress = false;
}

bool USTUWeaponComponent::CanFire() const
{
    return CurrentWeapon && !bEquipAnimInProgress && !bReloadAnimInProgress;
}

bool USTUWeaponComponent::CanEquip() const
{
    return !bEquipAnimInProgress;
}

bool USTUWeaponComponent::CanReload() const
{
    return CurrentWeapon              //
           && !bEquipAnimInProgress   //
           && !bReloadAnimInProgress  //
           && CurrentWeapon->CanReload();
}

void USTUWeaponComponent::OnEmptyClip(ASTUWeaponBase* AmmoEmptyWeapon)
{
    if (!AmmoEmptyWeapon) return;

    if (AmmoEmptyWeapon == CurrentWeapon)
    {
        ChangeClip();
    }
    else
    {
        for (const auto Weapon : Weapons)
        {
            if (Weapon == AmmoEmptyWeapon)
            {
                Weapon->ChangeClip();
            }
        }
    }
}

void USTUWeaponComponent::ChangeClip()
{
    if (!GetWorld() && !CanReload()) return;
    CurrentWeapon->StopFire();
    CurrentWeapon->ChangeClip();
    bReloadAnimInProgress = true;
    ReloadAnimTime = PlayAnimMontage(CurrentReloadAnimMontage);

    /* GetWorld()->GetTimerManager().SetTimer(
        ReloadTimerHandle,  //
        [this]()
        {
            ReloadAnimTime -= GetWorld()->GetDeltaSeconds();
            if (ReloadAnimTime <= 0.0f)
            {
                bReloadAnimInProgress = false;
                ReloadAnimTime = 0.0f;

                GetWorld()->GetTimerManager().ClearTimer(ReloadTimerHandle);
            }
        },                              //
        GetWorld()->GetDeltaSeconds(),  //
        true);*/

    //bReloadAnimInProgress = false;
}

bool USTUWeaponComponent::TryToAddAmmo(int32 ClipsAmount, TSubclassOf<ASTUWeaponBase> WeaponType)
{
    for (const auto Weapon : Weapons)
    {
        if (Weapon && Weapon->IsA(WeaponType))
        {
            return Weapon->TryToAddAmmo(ClipsAmount);
        }
    }
    return false;
}

bool USTUWeaponComponent::NeedAmmo(TSubclassOf<ASTUWeaponBase> WeaponType)
{
    for (const auto Weapon : Weapons)
    {
        if (Weapon && Weapon->IsA(WeaponType))
        {
            return !Weapon->IsAmmoFull();
        }
    }
    return false;
}

void USTUWeaponComponent::Zoom(bool Enabled)
{
    if (CurrentWeapon)
    {
        CurrentWeapon->Zoom(Enabled);
    }
}

bool USTUWeaponComponent::GetCurrentWeaponUIData(FWeaponUIData& UIData) const
{
    if (CurrentWeapon)
    {
        UIData = CurrentWeapon->GetUIData();
        return true;
    }
    return false;
}

bool USTUWeaponComponent::GetCurrentWeaponAmmoData(FAmmoData& AmmoData, bool bNeedNumberClips) const
{
    if (CurrentWeapon)
    {
        AmmoData = CurrentWeapon->GetCurrentAmmoData();

        if (!bNeedNumberClips)
        {
            AmmoData.Clips *= CurrentWeapon->GetDefaultAmmoData().Bullets;
        }

        return true;
    }

    return false;
}
