// Shoot Them Up Game. All Rights Reseved.

#include "STUCharacterBase.h"
// #include "Camera/CameraComponent.h"
// #include "Components/InputComponent.h"
// #include "Components/TextRenderComponent.h"
#include "Components/CapsuleComponent.h"
// #include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "CharacterComponent/STUCharacterMovementComponent.h"
#include "CharacterComponent/STUHealthComponent.h"
#include "CharacterComponent/STUWeaponComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(LogCharacterBase, All, All);

// Sets default values
ASTUCharacterBase::ASTUCharacterBase(const FObjectInitializer& ObjInit)
    : Super(ObjInit.SetDefaultSubobjectClass<USTUCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    HealthComponent = CreateDefaultSubobject<USTUHealthComponent>("HealthComponent");

    // HealthTextComponent = CreateDefaultSubobject<UTextRenderComponent>("HealthTextComponent");
    // HealthTextComponent->SetupAttachment(GetRootComponent());
    // HealthTextComponent->bOwnerNoSee = true;

    WeaponComponent = CreateDefaultSubobject<USTUWeaponComponent>("WeaponComponent");
}

// Called when the game starts or when spawned
void ASTUCharacterBase::BeginPlay()
{
    Super::BeginPlay();

    check(HealthComponent);
    // check(HealthTextComponent);
    check(GetCharacterMovement());
    check(GetMesh());

    OnHealthChange(HealthComponent->GetHealth(), 0.0f);
    HealthComponent->OnDeath.AddUObject(this, &ASTUCharacterBase::OnDeath);
    HealthComponent->OnHealthChange.AddUObject(this, &ASTUCharacterBase::OnHealthChange);

    LandedDelegate.AddDynamic(this, &ASTUCharacterBase::OnGroundLanded);
}

// Called every frame
void ASTUCharacterBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

bool ASTUCharacterBase::IsSprinting() const
{
    return false;
}

float ASTUCharacterBase::GetMovementDirection() const
{
    if (GetVelocity().IsZero()) return 0;

    const auto VelocityNormal = GetVelocity().GetSafeNormal();
    const auto AngleBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));
    const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);
    const auto Degrees = FMath::RadiansToDegrees(AngleBetween);

    return CrossProduct.IsZero() ? Degrees : Degrees * FMath::Sign(CrossProduct.Z);
}

void ASTUCharacterBase::SetPlayerColor(const FLinearColor Color)
{
    const auto MaterialInst = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
    if (!MaterialInst) return;

    MaterialInst->SetVectorParameterValue(MaterialColorName, Color);
}

void ASTUCharacterBase::OnHealthChange(float Health, float HealthDelta)
{
    // HealthTextComponent->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), Health)));
}

void ASTUCharacterBase::OnDeath()
{
    UE_LOG(LogCharacterBase, Display, TEXT("Player %s is dead"), *this->GetName());

    // PlayAnimMontage(AnimDeath);

    GetCharacterMovement()->DisableMovement();

    SetLifeSpan(5.0f);

    GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    WeaponComponent->StopFire();
    WeaponComponent->Zoom(false);

    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetMesh()->SetSimulatePhysics(true);

    if (GetWorld())
        UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
}

void ASTUCharacterBase::OnGroundLanded(const FHitResult& HitResult)
{
    const auto FallVelocityZ = -GetCharacterMovement()->Velocity.Z;
    UE_LOG(LogCharacterBase, Display, TEXT("On landed: %.2f"), FallVelocityZ);

    if (FallVelocityZ < LandedDamageVelocity.X) return;

    const auto FinalDamage = FMath::GetMappedRangeValueClamped(LandedDamageVelocity, LandedDamage, FallVelocityZ);
    UE_LOG(LogCharacterBase, Display, TEXT("Final damage: %.2f"), FinalDamage);
    TakeDamage(FinalDamage, FDamageEvent{}, nullptr, nullptr);
}
