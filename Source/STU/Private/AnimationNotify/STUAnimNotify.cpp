// Shoot Them Up Game. All Rights Reseved.


#include "AnimationNotify/STUAnimNotify.h"

void USTUAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
    OnNotifiedSignature.Broadcast(MeshComp);
    Super::Notify(MeshComp, Animation);
}
