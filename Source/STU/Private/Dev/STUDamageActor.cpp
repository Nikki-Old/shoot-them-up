// Shoot Them Up Game. All Rights Reseved.

#include "Dev/STUDamageActor.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASTUDamageActor::ASTUDamageActor()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
    SetRootComponent(SceneComponent);
}

// Called when the game starts or when spawned
void ASTUDamageActor::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void ASTUDamageActor::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    // Draw a sphere:
    DrawDebugSphere(GetWorld(), GetActorLocation(), Radius, 24, SphereColor);
    // Set damage: 
    UGameplayStatics::ApplyRadialDamage(GetWorld(), Damage, GetActorLocation(), Radius, DamageType, {}, this, nullptr, DoFullDamage);
}
